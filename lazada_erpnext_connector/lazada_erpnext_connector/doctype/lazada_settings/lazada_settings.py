# -*- coding: utf-8 -*-
# Copyright (c) 2020, Raaj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import requests
import time
import hmac
import hashlib
import json
import mimetypes
import itertools
import random
import logging
import os
from os.path import expanduser
import socket
import platform
from datetime import datetime
from frappe.utils import now,getdate,add_days
from frappe import enqueue

class LazadaSettings(Document):

    def get_access_token(self):
        auth = Authentication()
        auth.get_access_token()
    def get_refresh_token(self):
        auth = Authentication()
        auth.get_refresh_token()
    def get_products(self):
        prod = Products()
        prod.create_erpnext_items()
    def get_orders(self):
        ord = Orders()
        ord.create_erpnext_order()
    def get_transactions(self):
        trans = Transaction()
        trans.create_erpnext_jornal_entry()
    def get_shippment_pro(self):
        ship_pro = Delivery()
        frappe.msgprint(str(ship_pro.get_shippment_provider()))

# *****************************************************************************************************************
# API Functions Classes
# *****************************************************************************************************************

class Products(LazadaSettings):
    def __init__(self):
        self.api_key = frappe.db.get_value("Lazada Settings",None,"api_key")
        self.api_secret = frappe.db.get_value("Lazada Settings",None,"api_secret")
        self.access_token = frappe.db.get_value("Lazada Settings",None,"access_token")
        self.url = frappe.db.get_value("Lazada Settings",None,"url")
        self.last_sync_prod = frappe.db.get_value("Lazada Settings",None,"product_last_sync")
        self.sync_limit = frappe.db.get_value("Lazada Settings",None,"sync_limit")
        self.synced_items = frappe.db.get_value("Lazada Settings",None,"synced_items")

    def get_all_products(self,limit,offset):
        client = LazopClient(self.url, self.api_key ,self.api_secret)
        request = LazopRequest('/products/get','GET')
        request.add_api_param('filter', 'all')
        request.add_api_param('limit', 100)
        # request.add_api_param('offset', int(offset))
        if self.last_sync_prod:
            request.add_api_param('create_after', self.last_sync_prod)
        response = client.execute(request, self.access_token)
        if response.code != '0':
            # frappe.msgprint("Call is not sucessful")
            create_error_log('/products/get',response.code,response.message)
            return False
        else:
            frappe.msgprint(str(response.code))
            frappe.msgprint(str(response.body))
          
            return (response.body['data']['total_products'],response.body)
    
  

    def create_erpnext_items(self):
        lazada_product = self.get_all_products(self.sync_limit,self.synced_items)
        frappe.db.set_value("Lazada Settings",None,"total_lazada_item",int(lazada_product[0]))
        products = lazada_product[1]

        if products and products['data']:
            count = 0
            for product in products['data']['products']:
                for sku in product['skus']:
                    if not frappe.db.exists("Item", sku['SellerSku']):
                        if not frappe.db.exists("Item Group", product['primary_category']):
                            item_group = frappe.new_doc("Item Group")
                            item_group.item_group_name = str(product['primary_category'])
                            item_group.is_group = 0
                            item_group.parent_item_group = "Lazada Item Category"
                            item_group.insert(ignore_permissions=True)

                        item_doc = frappe.new_doc("Item")
                        item_doc.item_code = str(sku['SellerSku'])
                        item_doc.item_name = product['attributes']['name']
                        item_doc.description = product['attributes']['short_description']
                        item_doc.item_group = product['primary_category']
                        item_doc.stock_uom = "Nos"
                        item_doc.is_stock_item = 1
                        item_doc.valuation_rate = 1
                        item_doc.opening_stock = sku['quantity']
                        item_doc.insert(ignore_permissions=True)
                        item_doc.item_defaults = [{
                            "company":"Raaj Tailor",
                            "default_warehouse":frappe.db.get_value("Lazada Defaults",None,"default_warehouse")
                        }]
                        frappe.msgprint(str(item_doc.name)+" Created!")
                        # count = count + 1
            # frappe.db.set_value("Lazada Settings",None,"synced_items",int(frappe.db.get_value("Lazada Settings",None,"synced_items")) + count)
            # frappe.db.set_value("Lazada Settings",None,"remaining_items",int(frappe.db.get_value("Lazada Settings",None,"total_lazada_item")) -int(frappe.db.get_value("Lazada Settings",None,"synced_items")))
            frappe.db.set_value("Lazada Settings",None,"product_last_sync",datetime.now().replace(microsecond=0).isoformat())

class Delivery(object):
    def __init__(self):
        self.api_key = frappe.db.get_value("Lazada Settings",None,"api_key")
        self.api_secret = frappe.db.get_value("Lazada Settings",None,"api_secret")
        self.access_token = frappe.db.get_value("Lazada Settings",None,"access_token")
        self.url = frappe.db.get_value("Lazada Settings",None,"url")
    def get_shippment_provider(self):
        client = LazopClient(self.url, self.api_key ,self.api_secret)
        request = LazopRequest('/shipment/providers/get','GET')

        response = client.execute(request, self.access_token)
        data = response.body['data']['shipment_providers']
        for ship in data:
            shipment_provider = {
                "doctype":"Shipment Provider",
                "shipment_provider":ship['name'],
                "is_cod":ship['cod']
            }
            if not frappe.db.exists("Shipment Provider", ship['name']):
                frappe.get_doc(shipment_provider).insert(ignore_permissions=True)
        frappe.db.set_value("Lazada Settings",None,"last_shipment_provider_sync",datetime.now().replace(microsecond=0).isoformat())
        return response.body
        # print(response.type)
        # print(response.body)                 

class Orders(object):
    def __init__(self):
        self.api_key = frappe.db.get_value("Lazada Settings",None,"api_key")
        self.api_secret = frappe.db.get_value("Lazada Settings",None,"api_secret")
        self.access_token = frappe.db.get_value("Lazada Settings",None,"access_token")
        self.url = frappe.db.get_value("Lazada Settings",None,"url")
        self.default_customer = frappe.db.get_value("Lazada Defaults",None,"customer")
        self.default_warehouse = frappe.db.get_value("Lazada Defaults",None,"default_warehouse")
        self.last_sync = frappe.db.get_value("Lazada Settings",None,"order_last_sync")

    def get_all_orders(self):
        client = LazopClient(self.url, self.api_key ,self.api_secret)
        request = LazopRequest('/orders/get','GET')
        request.add_api_param('created_after', self.last_sync)
        request.add_api_param('status', 'pending')
        request.add_api_param('limit', 50)
        request.add_api_param('sort_direction', 'DESC')
        frappe.msgprint(str(self.access_token))
        response = client.execute(request,self.access_token)
        # frappe.msgprint(str(response.type))
        # frappe.msgprint(str(response.body))
        if response.code != '0':
            # frappe.msgprint("Call is not sucessful")
            create_error_log('/orders/get',response.code,response.message)
            return False
        else:
            # frappe.msgprint(str(response.code))
            # frappe.msgprint(str(response.body))
            return response.body
        
    
    def get_order_items(self,order_id):
        client = LazopClient(self.url, self.api_key ,self.api_secret)
        request = LazopRequest('/orders/items/get','GET')
        request.add_api_param('order_ids', order_id)
        response = client.execute(request, self.access_token)
        if response.code != '0':
            # frappe.msgprint("Call is not sucessful")
            create_error_log('/orders/items/get',response.code,response.message)
            return False
        else:
            frappe.msgprint(str(response.code))
            frappe.msgprint(str(response))
            # res_body = response.body
            return response.body
        # return response.body


    
    def create_erpnext_order(self):
        orders_list = self.get_all_orders()
        orders_ids=[]
        if orders_list:
            for order in orders_list['data']['orders']:
                orders_ids.append(str(order['order_id']))
            order_items = self.get_order_items(str(orders_ids))
            if order_items:
                for order in orders_list['data']['orders']:
                    for order_item in order_items['data']:
                        if order['order_id'] == order_item['order_id']:
                            items = []
                            for item in order_item['order_items']:
                                items.append({
                                    "item_code":item['sku'],
                                    "item_name":item["name"],
                                    "rate":item['item_price'],
                                    "qty":1,
                                    "order_item_id":item['order_item_id']
                                })
                            sales_order = {
                            "doctype":"Sales Order",
                            "customer":self.default_customer,
                            "set_warehouse":self.default_warehouse,
                            "transaction_date":getdate(datetime.strptime(order['created_at'], "%Y-%m-%d %H:%M:%S %z")),
                            "delivery_date":add_days(getdate(datetime.strptime(order['created_at'], "%Y-%m-%d %H:%M:%S %z")),2),
                            "po_no":order['order_number'],
                            "items":items,
                            "cash_direct_sales_name":order['address_billing']['first_name'],
                            "order_type":"Sales"}
                            if not frappe.db.exists("Sales Order", frappe.db.get_value("Sales Order",{"po_no":order['order_number']},"name")):
                                frappe.msgprint(str(items))
                                frappe.msgprint(str(sales_order))
                                frappe.get_doc(sales_order).insert(ignore_permissions=True)
                
            frappe.db.set_value("Lazada Settings",None,"order_last_sync",datetime.now().replace(microsecond=0).isoformat())

class OrdersDoc(object):
    def __init__(self):
        self.api_key = frappe.db.get_value("Lazada Settings",None,"api_key")
        self.api_secret = frappe.db.get_value("Lazada Settings",None,"api_secret")
        self.access_token = frappe.db.get_value("Lazada Settings",None,"access_token")
        self.url = frappe.db.get_value("Lazada Settings",None,"url")

    def get_oreder_doc(self,order_item_ids):
        client = LazopClient(self.url, self.api_key ,self.api_secret)
        request = LazopRequest('/order/document/get','GET')
        request.add_api_param('doc_type', 'invoice')
        request.add_api_param('order_item_ids', order_item_ids)
        
        response = client.execute(request, self.access_token)
        # frappe.msgprint(str(response.type))
        # frappe.msgprint(str(response.body))
        return response.body
    
    def attach_to_erpnext_invoice(self):
        pass
        
class Transaction(object):
    def __init__(self):
        self.api_key = frappe.db.get_value("Lazada Settings",None,"api_key")
        self.api_secret = frappe.db.get_value("Lazada Settings",None,"api_secret")
        self.access_token = frappe.db.get_value("Lazada Settings",None,"access_token")
        self.url = frappe.db.get_value("Lazada Settings",None,"url")
        self.company = frappe.db.get_value("Global Defaults",None,"default_company")
        self.cash_account = frappe.db.get_value("Company",self.company,"default_cash_account")
        self.receivalble = frappe.db.get_value("Company",self.company,"default_receivable_account")
        self.customer = frappe.db.get_value("Lazada Defaults",None,"customer")
        self.last_transc_sync = frappe.db.get_value("Lazada Defaults",None,"transaction_last_sync")
        self.from_date = frappe.db.get_value("Lazada Settings",None,"from_date")
        self.to_date = frappe.db.get_value("Lazada Settings",None,"to_date")

    def get_all_transaction(self):
        # frappe.msgprint(str(self.from_date))
        # frappe.throw(str(self.to_date))
        if not self.from_date or not self.to_date:
            frappe.throw("Please Enter <b>From Date</b> and <b>To Date</b> to get Transaction.")
        client = LazopClient(self.url, self.api_key ,self.api_secret)
        request = LazopRequest('/finance/transaction/detail/get','GET')
        request.add_api_param('trans_type', '13')
        request.add_api_param('end_time', str(self.to_date))
        request.add_api_param('start_time',str(self.from_date))
        response = client.execute(request, self.access_token)
        # frappe.msgprint(str(response.type))
        # frappe.msgprint(str(response.body))
        return response.body
    
    def create_erpnext_jornal_entry(self):
        transaction_list = self.get_all_transaction()
        account_entry = []
        invoice_list = []

        for transc in transaction_list['data']:
            sales_invoice = frappe.db.get_value("Sales Invoice",{"po_no":transc["order_no"],"je_created":0},"name")
            if sales_invoice:
                account_entry.append({
                    "account":self.receivalble,
                    "party_type":"Customer",
                    "party":self.customer,
                    "credit_in_account_currency":float(transc['amount']),
                    "reference_type":"Sales Invoice",
                    "reference_name":sales_invoice
                })
                account_entry.append({
                    "account":self.cash_account,
                    "debit_in_account_currency":float(transc['amount']),
                    
                })
                invoice_list.append(sales_invoice)
        
        if len(account_entry) != 0:
            frappe.msgprint(str(account_entry))
            self.creat_je(account_entry,invoice_list)
        frappe.db.set_value("Lazada Settings",None,"transaction_last_sync",datetime.now().replace(microsecond=0).isoformat())
    
    def creat_je(self,account_entry,invoice_list):
        je_doc ={
            "doctype":"Journal Entry",
            "voucher_type":"Journal Entry",
            "posting_date":frappe.utils.now(),
            "accounts":account_entry
        }
        frappe.get_doc(je_doc).insert(ignore_permissions=True)

        for inv in invoice_list:
            frappe.db.set_value("Sales Invoice",inv,"je_created",1)
            frappe.msgprint("Payment Created For Invoice {}".format(inv))
        
class Authentication(object):
    def __init__(self):
        self.api_key = frappe.db.get_value("Lazada Settings",None,"api_key")
        self.api_secret = frappe.db.get_value("Lazada Settings",None,"api_secret")
        self.callback_url = frappe.db.get_value("Lazada Settings",None,"callback_url")
        self.url = frappe.db.get_value("Lazada Settings",None,"url")
        self.refresh_token = frappe.db.get_value("Lazada Settings",None,"refresh_token")
        
    def get_code(self):
        res = frappe.db.get_value("Lazada Settings",None,"code")
        # res = requests.get("https://auth.lazada.com/oauth/authorize?response_type=code&force_auth=true&redirect_uri={call_back_url}&client_id={appkey}".format(call_back_url=self.callback_url,appkey=self.api_key))
        # frappe.msgprint(str(res))
        return res

    def get_access_token(self):
        client = LazopClient("https://auth.lazada.com/rest", self.api_key ,self.api_secret)
        request = LazopRequest('/auth/token/create')
        request.add_api_param("code", self.get_code())
        response = client.execute(request)
        frappe.db.set_value("Lazada Settings",None,"access_token",response.body["access_token"])
        frappe.db.set_value("Lazada Settings",None,"refresh_token",response.body['refresh_token'])
        
        self.access_token = response.body["access_token"]
        self.refresh_token = response.body['refresh_token']
        
        frappe.msgprint(str(response.body))
    def get_refresh_token(self):
        client = LazopClient("https://auth.lazada.com/rest", self.api_key ,self.api_secret)
        request = LazopRequest('/auth/token/refresh')
        request.add_api_param("refresh_token", self.refresh_token)
        response = client.execute(request)
        frappe.msgprint(str(response.body))
        frappe.msgprint(str(now()))
        frappe.db.set_value("Lazada Settings",None,"access_token",response.body["access_token"])
        frappe.db.set_value("Lazada Settings",None,"refresh_token",response.body['refresh_token'])
        frappe.db.set_value("Lazada Settings",None,"last_sync_access_token",now())
        self.access_token = response.body["access_token"]
        self.refresh_token = response.body['refresh_token']
        self.last_sync_access_token = now()

# *********************************************************************************************************************
# FunctionS called By Scheduler
# *********************************************************************************************************************

def create_error_log(call,error_code,error):
    doc = frappe.new_doc("Lazada Connector Error Log")
    doc.call=call
    doc.error_code=error_code
    doc.error_log=error
    doc.insert(ignore_permissions=True)

frappe.whitelist()
def get_refresh():
    auth = Authentication()
    auth.get_refresh_token()

frappe.whitelist()
def get_orders():
    ord = Orders()
    ord.create_erpnext_order()

frappe.whitelist()
def get_items_back():
    prod = Products()
    prod.create_erpnext_items()

# ***************************************************************************************************************
# Lazada API Functions
# ***************************************************************************************************************

dir = expanduser("~")
isExists = os.path.exists(dir + "/logs")
if not isExists:
    os.makedirs(dir + "/logs") 
logger = logging.getLogger(__name__)
logger.setLevel(level = logging.ERROR)
handler = logging.FileHandler(dir + "/logs/lazopsdk.log." + time.strftime("%Y-%m-%d", time.localtime()))
handler.setLevel(logging.ERROR)

formatter = logging.Formatter('%(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

P_SDK_VERSION = "lazop-sdk-python-20181207"

P_APPKEY = "app_key"
P_ACCESS_TOKEN = "access_token"
P_TIMESTAMP = "timestamp"
P_SIGN = "sign"
P_SIGN_METHOD = "sign_method"
P_PARTNER_ID = "partner_id"
P_DEBUG = "debug"

P_CODE = 'code'
P_TYPE = 'type'
P_MESSAGE = 'message'
P_REQUEST_ID = 'request_id'

P_API_GATEWAY_URL_SG = 'https://api.lazada.sg/rest'
P_API_GATEWAY_URL_MY = 'https://api.lazada.com.my/rest'
P_API_GATEWAY_URL_VN = 'https://api.lazada.vn/rest'
P_API_GATEWAY_URL_TH = 'https://api.lazada.co.th/rest'
P_API_GATEWAY_URL_PH = 'https://api.lazada.com.ph/rest'
P_API_GATEWAY_URL_ID = 'https://api.lazada.co.id/rest'
P_API_AUTHORIZATION_URL = 'https://auth.lazada.com/rest'

P_LOG_LEVEL_DEBUG = "DEBUG"
P_LOG_LEVEL_INFO = "INFO"
P_LOG_LEVEL_ERROR = "ERROR"


def sign(secret,api, parameters):
    #===========================================================================
    # @param secret
    # @param parameters
    #===========================================================================
    sort_dict = sorted(parameters)
    
    parameters_str = "%s%s" % (api,
        str().join('%s%s' % (key, parameters[key]) for key in sort_dict))

    h = hmac.new(secret.encode(encoding="utf-8"), parameters_str.encode(encoding="utf-8"), digestmod=hashlib.sha256)

    return h.hexdigest().upper()


def mixStr(pstr):
    if(isinstance(pstr, str)):
        return pstr
    elif(isinstance(pstr, str)):
        return pstr.encode('utf-8')
    else:
        return str(pstr)

def logApiError(appkey, sdkVersion, requestUrl, code, message):
    localIp = socket.gethostbyname(socket.gethostname())
    platformType = platform.platform()
    logger.error("%s^_^%s^_^%s^_^%s^_^%s^_^%s^_^%s^_^%s" % (
        appkey, sdkVersion,
        time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()),
        localIp, platformType, requestUrl, code, message))

class LazopRequest(object):
    def __init__(self,api_pame,http_method = 'POST'):
        self._api_params = {}
        self._file_params = {}
        self._api_pame = api_pame
        self._http_method = http_method

    def add_api_param(self,key,value):
        self._api_params[key] = value

    def add_file_param(self,key,value):
        self._file_params[key] = value


class LazopResponse(object):
    def __init__(self):
        self.type = None
        self.code = None
        self.message = None
        self.request_id = None
        self.body = None
    
    def __str__(self, *args, **kwargs):
        sb = "type=" + mixStr(self.type) +\
            " code=" + mixStr(self.code) +\
            " message=" + mixStr(self.message) +\
            " requestId=" + mixStr(self.request_id)
        return sb

class LazopClient(object):
    
    log_level = P_LOG_LEVEL_ERROR
    def __init__(self, server_url,app_key,app_secret,timeout=30):
        self._server_url = server_url
        self._app_key = app_key
        self._app_secret = app_secret
        self._timeout = timeout
    
    def execute(self, request,access_token = None):

        sys_parameters = {
            P_APPKEY: self._app_key,
            P_SIGN_METHOD: "sha256",
            P_TIMESTAMP: str(int(round(time.time()))) + '000',
            P_PARTNER_ID: P_SDK_VERSION
        }

        if(self.log_level == P_LOG_LEVEL_DEBUG):
            sys_parameters[P_DEBUG] = 'true'

        if(access_token):
            sys_parameters[P_ACCESS_TOKEN] = access_token

        application_parameter = request._api_params;

        sign_parameter = sys_parameters.copy()
        sign_parameter.update(application_parameter)

        sign_parameter[P_SIGN] = sign(self._app_secret,request._api_pame,sign_parameter)

        api_url = "%s%s" % (self._server_url,request._api_pame)

        full_url = api_url + "?";
        for key in sign_parameter:
            full_url += key + "=" + str(sign_parameter[key]) + "&";
        full_url = full_url[0:-1]

        try:
            if(request._http_method == 'POST' or len(request._file_params) != 0) :
                r = requests.post(api_url,sign_parameter,files=request._file_params, timeout=self._timeout)
            else:
                r = requests.get(api_url,sign_parameter, timeout=self._timeout)
        except Exception as err:
            logApiError(self._app_key, P_SDK_VERSION, full_url, "HTTP_ERROR", str(err))
            raise err

        response = LazopResponse()

        jsonobj = r.json()

        if P_CODE in jsonobj:
            response.code = jsonobj[P_CODE]
        if P_TYPE in jsonobj:
            response.type = jsonobj[P_TYPE]
        if P_MESSAGE in jsonobj:
            response.message = jsonobj[P_MESSAGE]
        if P_REQUEST_ID in jsonobj:
            response.request_id = jsonobj[P_REQUEST_ID]

        if response.code is not None and response.code != "0":
            logApiError(self._app_key, P_SDK_VERSION, full_url, response.code, response.message)
        else:
            if(self.log_level == P_LOG_LEVEL_DEBUG or self.log_level == P_LOG_LEVEL_INFO):
                logApiError(self._app_key, P_SDK_VERSION, full_url, "", "")

        response.body = jsonobj

        return response

